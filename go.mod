module main.go

go 1.12

require (
	github.com/aws/aws-lambda-go v1.13.0
	github.com/go-pg/pg/v9 v9.0.0-beta.7
	github.com/jinzhu/gorm v1.9.10
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.2.0
	github.com/stretchr/testify v1.3.0
)
