package model

import "github.com/jinzhu/gorm"

type Thing struct {
	gorm.Model
	Name string `json:"name"`
}
