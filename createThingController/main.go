package main

import (
	"context"

	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/lib/pq"
	"main.go/db"
	model "main.go/models"
)

type CreateThingRequest struct {
	Name     string `json:"name"`
	DbNumber int8   `json:"dbNumber`
}

func HandleRequest(
	ctx context.Context,
	request CreateThingRequest) (model.Thing, error) {
	postgresConnector := db.PostgresConnector{}
	db, err := postgresConnector.GetConnection(request.DbNumber)
	defer db.Close()
	if err != nil {
		return model.Thing{}, err
	}
	db.AutoMigrate(&model.Thing{})
	thing := &model.Thing{}
	thing.Name = request.Name
	db.Create(thing)
	return *thing, nil
}

func main() {
	lambda.Start(HandleRequest)
}
