package main

import (
	"context"

	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/lib/pq"
	"main.go/db"
	model "main.go/models"
)

type GetThingRequest struct {
	ID       uint `json:"id"`
	DbNumber int8 `json:"dbNumber`
}

func HandleRequest(
	ctx context.Context,
	request GetThingRequest) ([]model.Thing, error) {
	postgresConnector := db.PostgresConnector{}
	db, err := postgresConnector.GetConnection(request.DbNumber)
	defer db.Close()
	if err != nil {
		return []model.Thing{}, err
	}
	db.AutoMigrate(&model.Thing{})
	thing := &model.Thing{}
	thing.ID = request.ID
	var things []model.Thing
	db.Where(thing).Find(&things)
	return things, nil
}

func main() {
	lambda.Start(HandleRequest)
}
