package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandleRequest(t *testing.T) {
	things, _ := HandleRequest(GetThingRequest{
		ID: 1,
	})

	assert.NotEqual(t, 0, len(things))
	assert.Equal(t, "Stuff", things[0].Name)
}
