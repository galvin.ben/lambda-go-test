package db

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

type PostgresConnector struct {
}

func (p *PostgresConnector) GetConnection(dbNumber int8) (db *gorm.DB, err error) {
	e := godotenv.Load()
	if e != nil {
		fmt.Print(e)
	}

	username := os.Getenv(fmt.Sprintf("db%v_user", dbNumber))
	password := os.Getenv(fmt.Sprintf("db%v_pass", dbNumber))
	dbName := os.Getenv(fmt.Sprintf("db%v_name", dbNumber))
	dbHost := os.Getenv(fmt.Sprintf("db%v_host", dbNumber))

	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password)
	return gorm.Open("postgres", dbURI)
}
